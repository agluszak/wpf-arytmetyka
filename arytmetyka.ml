(* Autor: Andrzej Głuszak 385527*)
(* Reviewer: Piotr Storożenko *)

(* Przedzial moze byc pusty, "normalny", może też zawierać "dziurę" (antyprzedział) *)

(* definicja typu *)
type wartosc =
  | Przedzial of float * float
  | Antyprzedzial of float * float
  | Pusty

(* konstruktory *)
let wartosc_dokladnosc x p =
  assert (p > 0.);
  let percent = abs_float (p *. x /. 100.0) in
  Przedzial (x -. percent, x +. percent)

let wartosc_od_do x y =
  assert (x <= y);
  Przedzial (x, y)

let wartosc_dokladna x =
  Przedzial (x, x)

(* funkcje pomocnicze *)
let nieskonczony_float f =
  f = neg_infinity || f = infinity

let czy_nan x =
  compare x nan = 0

(* zwraca mniejsza wartosc ktora nie jest nanem *)
let min_nan a b =
  if czy_nan a then b
  else if czy_nan b then a
  else min a b

(* zwraca wiekza wartosc ktora nie jest nanem *)
let max_nan a b =
  if czy_nan a then b
  else if czy_nan b then a
  else max a b

(* zwraca najmniejsza wartosc z 4 liczb ktora nie jest nanem *)
let minimum a b c d =
  min_nan (min_nan a b) (min_nan c d)

(* zwraca najwieksza wartosc z 4 liczb ktora nie jest nanem *)
let maksimum a b c d =
  max_nan (max_nan a b) (max_nan c d)

(* zwraca wynik najmniejszego mnozenia koncow dwoch przedzialow*)
let najmniejsze_mnozenie a b c d =
  minimum (a *. c) (a *. d) (b *. c) (b *. d)

(* zwraca wynik najwiekszego mnozenia koncow dwoch przedzialow*)
let najwieksze_mnozenie a b c d =
  maksimum (a *. c) (a *. d) (b *. c) (b *. d)

(* selektory *)
let min_wartosc w =
  match w with
  | Przedzial (a, _) -> a
  | Antyprzedzial (_, _) -> neg_infinity
  | Pusty -> nan

let max_wartosc w =
  match w with
  | Przedzial (_, b) -> b
  | Antyprzedzial (_, _) -> infinity
  | Pusty -> nan

let sr_wartosc w =
  match w with
  | Przedzial (a, b) when (nieskonczony_float a) && (nieskonczony_float b) ->
    nan
  | Przedzial (a, b) when (a = neg_infinity) ->
    neg_infinity
  | Przedzial (a, b) when (b = infinity) ->
    infinity
  | Przedzial (a, b) -> (a +. b) /. 2.
  | Antyprzedzial (_,_) -> nan
  | Pusty -> nan

let in_wartosc w x =
  match w with
  | Przedzial (a, b) -> (x >= a) && (x <= b)
  | Antyprzedzial (a, b) -> (x <= a) || (x >= b)
  | Pusty -> false

(* pomocnicze  *)
(* oblicza 1/w *)
let odwrotnosc w =
  match w with
  | Przedzial (a, b) when a = neg_infinity && b = infinity ->
    Przedzial (neg_infinity, infinity)
  | Pusty ->
    Pusty
  | Antyprzedzial (a, b) when a = b ->
    Przedzial (neg_infinity, infinity)
  | Przedzial (0., 0.) ->
    Pusty
  | Przedzial (0., b) ->
    Przedzial (1. /. b, infinity)
  | Przedzial (a, 0.) ->
    Przedzial (neg_infinity, 1. /. a)
  | Przedzial (a, b) when in_wartosc w 0. ->
    Antyprzedzial (1. /. a, 1. /. b)
  | Przedzial (a, b) ->
    Przedzial (min (1. /. a ) (1. /. b), max (1. /. a ) (1. /. b))
  | Antyprzedzial (a, b) when a < 0. && b > 0. ->
    Przedzial (min (1. /. a ) (1. /. b), max (1. /. a ) (1. /. b))
  | Antyprzedzial (a, b) ->
    Antyprzedzial (min (1. /. a ) (1. /. b), max (1. /. a ) (1. /. b))

(* oblicza 0 - w *)
let przeciwienstwo w =
  match w with
  | Pusty ->
    Pusty
  | Antyprzedzial (a, b) when a = b ->
    Przedzial (neg_infinity, infinity)
  | Antyprzedzial (a, b) ->
    Antyprzedzial (min (0. -. a) (0. -. b), max (0. -. a) (0. -. b))
  | Przedzial (a, b) ->
    Przedzial (min (0. -. a) (0. -. b), max (0. -. a) (0. -. b))

(* lewy podprzedzial antyprzedzialu *)
let lewy_przedzial w =
  match w with
  | Antyprzedzial (a, _) -> Przedzial (neg_infinity, a)
  | x -> x

(* prawy podprzedzial antyprzedzialu *)
let prawy_przedzial w =
  match w with
  | Antyprzedzial (_, b) -> Przedzial (b, infinity)
  | x -> x

(* suma mnogosciowa przedzialow *)
let rec suma x y =
  match (x, y) with
  | Przedzial (a, b), Przedzial (c, d) ->
    (if a = neg_infinity && d = infinity then
      (if c > b then Antyprzedzial (b, c)
      else Przedzial (neg_infinity, infinity))
    else if b = infinity && c = neg_infinity then
      (if a > d then Antyprzedzial (d, a)
      else Przedzial (neg_infinity, infinity))
    else Przedzial (min a c, max b d))
  | Przedzial (a, b), Antyprzedzial (c, d) ->
    suma (suma x (lewy_przedzial y)) (suma x (prawy_przedzial y))
  | Antyprzedzial (a, b), Przedzial (c, d) ->
    suma y x
  | Antyprzedzial (a, b), Antyprzedzial (c, d) ->
    Antyprzedzial (max a c, min b d)
  | Pusty, _ -> Pusty
  | _, Pusty -> Pusty

(* uogolnienie wykonywania dzialan na przedzialach *)
let rec dzialanie x y f =
  match (x, y) with
  | _, Pusty ->
    Pusty
  | Pusty, _ ->
    Pusty
  | Przedzial(_,_), Przedzial (_,_) ->
    f x y
  | Antyprzedzial(_,_), Przedzial(_,_) ->
    dzialanie y x f
  | Przedzial(_,_), Antyprzedzial(_,_) ->
    let w1 = dzialanie x (lewy_przedzial y) f
    and w2 = dzialanie x (prawy_przedzial y) f in
    suma w1 w2
  | Antyprzedzial(_,_), Antyprzedzial (_,_) ->
    let w1 = dzialanie (lewy_przedzial x) (lewy_przedzial y) f
    and w2 = dzialanie (lewy_przedzial x) (prawy_przedzial y) f
    and w3 = dzialanie (prawy_przedzial x) (lewy_przedzial y) f
    and w4 = dzialanie (prawy_przedzial x) (prawy_przedzial y) f in
    suma (suma w1 w2) (suma w3 w4)

(* modyfikatory *)
let rec plus x y =
  match (x, y) with
  | Przedzial (a, b), Przedzial (c, d) ->
    Przedzial (a +. c, b +. d)
  | a, b -> dzialanie a b plus

let minus x y =
  plus x (przeciwienstwo y)

let rec razy x y =
  match (x, y) with
  | Przedzial (a, b), Przedzial (c, d) ->
    if (a = 0. && b = 0.) || (c = 0. && d = 0.) then Przedzial (0.,0.) else
    Przedzial (najmniejsze_mnozenie a b c d , najwieksze_mnozenie a b c d)
  | a, b -> dzialanie a b razy

let podzielic x y =
  razy x (odwrotnosc y)
